package stepDefination;

import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import cucumber.api.CucumberOptions;
import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import junit.framework.Assert;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Managed;
import net.serenitybdd.cucumber.*;
import net.thucydides.core.annotations.Managed;

public class VisaRequirementSteps {

	@Managed
	WebDriver driver;

	@Given("^I provide a nationality of Japan$")
	public void i_provide_a_nationality_of_japan() throws Throwable {

		driver.get("https://www.gov.uk/check-uk-visa/y");
		driver.manage().window().maximize();
		Select selectdropdown = new Select(driver.findElement(By.id("response")));
		selectdropdown.selectByVisibleText("Japan");
		driver.findElement(By.xpath("//button[@class='gem-c-button govuk-button gem-c-button--bottom-margin']")).click();
	}

	@And("^I select the reason “Study”$")
	public void i_select_the_reason_study() throws Throwable {
		driver.findElement(By.xpath("//input[@value='study']")).click();
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("window.scrollBy(0,250)");
		driver.findElement(By.xpath("//button[@class='gem-c-button govuk-button gem-c-button--bottom-margin']")).click();
	}

	@And("^I state I am intending to stay for more than 6 months$")
	public void i_state_i_am_intending_to_stay_for_more_than_6_months() throws Throwable {
		driver.findElement(By.xpath("//input[@value='longer_than_six_months']")).click();
	}

	@When("^I submit the form$")
	public void i_submit_the_form() throws Throwable {
		driver.findElement(By.xpath("//button[@class='gem-c-button govuk-button gem-c-button--bottom-margin']")).click();
	}

	@Then("^I will be informed “I need a visa to study in the UK”$")
	public void i_will_be_informed_i_need_a_visa_to_study_in_the_uk() throws Throwable {
		WebElement element = driver.findElement(By.xpath("//h2[@class='gem-c-heading   ']"));
		String actual = element.getText();
		System.out.println(actual);
		Assert.assertEquals("You’ll need a visa to study in the UK", actual);
	}
//japan and reason
	@And("^I select the reason “Tourism”$")
	public void i_select_the_reason_tourism() throws Throwable {
		driver.findElement(By.xpath("//input[@value='tourism']")).click();
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("window.scrollBy(0,250)");
		}

	@Then("^I will be informed “I won’t need a visa to study in the UK”$")
	public void i_will_be_informed_i_wont_need_a_visa_to_study_in_the_uk() throws Throwable {
		
		WebElement element = driver.findElement(By.xpath("//h2[@class='gem-c-heading   ']"));
		String actual = element.getText();
		System.out.println(actual);
		Assert.assertEquals("You won’t need a visa to come to the UK", actual);
	}
	//Russia 
	 @Given("^I provide a nationality of Russia$")
	    public void i_provide_a_nationality_of_russia() throws Throwable {
		   driver.get("https://www.gov.uk/check-uk-visa/y");
			driver.manage().window().maximize();
			Select selectdropdown = new Select(driver.findElement(By.id("response")));
			selectdropdown.selectByVisibleText("Russia");
			driver.findElement(By.xpath("//button[@class='gem-c-button govuk-button gem-c-button--bottom-margin']")).click();
	    }
	  @And("^I state I am not travelling or visiting a partner or family$")
	    public void i_state_i_am_not_travelling_or_visiting_a_partner_or_family() throws Throwable {
		  driver.findElement(By.xpath("//button[@class='gem-c-button govuk-button gem-c-button--bottom-margin']")).click();
		  driver.findElement(By.xpath("//input[@value='no']")).click();
		  }
	    
	    @Then("^I will be informed “I need a visa to come to the UK”$")
	    public void i_will_be_informed_i_need_a_visa_to_come_to_the_uk() throws Throwable {
	    	WebElement element = driver.findElement(By.xpath("//h2[@class='gem-c-heading   ']"));
			String actual = element.getText();
			System.out.println(actual);
			Assert.assertEquals("You’ll need a visa to come to the UK", actual);
	    }

	   
	  

}
