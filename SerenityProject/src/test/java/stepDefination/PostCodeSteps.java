package stepDefination;

import io.restassured.RestAssured;
import io.restassured.RestAssured.*;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Step;
import static org.hamcrest.Matcher.*;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;


public class PostCodeSteps {
	
	@Managed
	WebDriver driver;
	 @When("^I send a get request to \"([^\"]*)\"$")
    public void i_send_a_get_request_to_something(String strArg1) throws Throwable {
		RestAssured.
        when().get("api.postcodes.io/postcodes/SW1P4JA");
	}
  
    @Then("^I get a 200 response$")
    public void i_get_a_200_response() throws Throwable {
    	RestAssured.
        when().get("api.postcodes.io/postcodes/SW1P4JA").
    	  then().assertThat().statusCode(200);
    
    }
       
	       
	
}
